﻿namespace SpaceMath
{
    public class IntegratorRK4
    {
        public delegate Vector3d RKFunctionDelegate(State state, double t);

        Derivative Evaluate (State initialState, double t, double dt, Derivative initialDeriv, RKFunctionDelegate rkfunc)
        {
            State state = new State();
            state.pos = initialState.pos + initialDeriv.dPos * dt;
            state.vel = initialState.vel + initialDeriv.dVel * dt;

            Derivative output = new Derivative();
            output.dPos = state.vel;
            output.dVel = rkfunc(state, t + dt);

            return output;
        }


        
        public void Integrate(ref State state, double t, double dt, RKFunctionDelegate rkfunc)
        {
            Derivative d1, d2, d3, d4;

            const double oneSixth = 1d / 6d;
            double oneHalfdt = dt / 2d;

            d1 = Evaluate(state, t, 0d, new Derivative(), rkfunc);
            d2 = Evaluate(state, t, oneHalfdt, d1, rkfunc);
            d3 = Evaluate(state, t, oneHalfdt, d2, rkfunc);
            d4 = Evaluate(state, t, dt, d3, rkfunc);

            Vector3d dPos_dt = oneSixth * (d1.dPos + 2d * (d2.dPos + d3.dPos) + d4.dPos);
            Vector3d dVel_dt = oneSixth * (d1.dVel + 2d * (d2.dVel + d3.dVel) + d4.dVel);

            state.pos += dPos_dt * dt;
            state.vel += dVel_dt * dt;
        }
    }
}
