﻿namespace SpaceMath
{
    public interface IAffector
    {
        double Affect(State state, double t);
    }
}