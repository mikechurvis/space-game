﻿
namespace SpaceMath
{
    /// <summary>
    ///     The state of an object in the physics sim.
    /// </summary>
    public struct State
    {
        /// <summary>
        ///     The current position of the object.
        /// </summary>
        public Vector3d pos;

        /// <summary>
        ///     The current velocity of the object.
        /// </summary>
        public Vector3d vel;
    }


    /// <summary>
    ///     The derivatives of position and velocity.
    /// </summary>
    public struct Derivative
    {
        /// <summary>
        ///     The derivative of position.
        /// </summary>
        public Vector3d dPos;

        /// <summary>
        ///     The derivative of velocity.
        /// </summary>
        public Vector3d dVel;
    }
}